import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import './i18n';

export default function Chapter() {
  const { chapterNumber } = useParams(); // This will get 'chapterNumber' from the URL
  const { t } = useTranslation();
  console.log('t :>> ', t(`chapter_${chapterNumber}.title`));
  return (
    <div>
      <h1>{t(`chapter_${chapterNumber}.title`)}</h1>
      <p>{t(`chapter_${chapterNumber}.content`)}</p>
    </div>
  );
}
