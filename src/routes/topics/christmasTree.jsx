import { CodeBlock, CopyBlock, monokai } from 'react-code-blocks';

export default function ChristmasTree() {
  return (
    <>
      <header>
        <h1>Weihnachtsbaum</h1>
      </header>
      {/*       <nav>
        <a href="#">Home</a>
        <a href="#">Articles</a>
        <a href="#">Contact</a>
        <a href="#">About Us</a>
      </nav> */}
      <article>
        <p>
          Das heutige Material dreht sich um Weihnachtsbäume und die Verwendung
          der Kommandozeile. Zumindest werden wir versuchen, einen
          Weihnachtsbaum auf der Konsole zu zeichnen.
        </p>
        <p>
          Wir werden mit der einfachsten Version dieser Übung beginnen und uns
          langsam an die Lösung herantasten. Die Hälfte eines Weihnachtsbaums
          darzustellen, sollte für niemanden schwierig sein.
        </p>
        <p>
          <CopyBlock
            text={`print("*")
print("**")
print("***")
print("*")
print("**")
print("***")
print("****")
print("*")
print("**")
print("***")
print("****")
print("*****")
print("******")`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          <CodeBlock
            text={`*
**
***
*
**
***
****
*
**
***
****
*****
******`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Es sieht nicht schlecht aus, aber es ist viel zu viel Schrift. Was,
          wenn wir einen kleineren Baum wollen? Oder einen größeren, der aus
          mehreren hundert Ebenen besteht und im A0-Format gedruckt wird. Das
          wäre definitiv zu viel Schreibarbeit, selbst wenn wir die
          Multiplikation von Zeichenketten (&quot;*&quot; * 100) verwenden
          würden. Wenn wir eine solche sich wiederholende Tätigkeit sehen,
          wissen wir, dass die naheliegende Wahl die Automatisierung ist.
        </p>
        <h2>Listen und eine for Schleife</h2>
        <p>
          Wir werden Schleifen verwenden, um mit wiederholten Aktionen
          umzugehen. Stellen Sie sich einen Moment lang vor, Sie wären der
          Weihnachtsmann und müssten die Weihnachtsgeschenke an alle ausliefern.
          Wie Sie wissen, hat der Weihnachtsmann eine Liste mit guten Menschen,
          die Geschenke verdient haben. Die einfachste Methode, um
          sicherzustellen, dass niemand vergessen wird, wäre, die Liste der
          Reihe nach abzuarbeiten und die Geschenke nacheinander auszuliefern,
          indem man sie vom magischen Schlitten fallen lässt. Abgesehen von den
          physischen Aspekten von Aufgabe 1 könnte das Verfahren für die
          Auslieferung der Geschenke zum Beispiel wie folgt aussehen:
        </p>
        <p>
          Let the People List contain people who should receive gifts. For each
          person (known as the Person), which is on the list of people: Provide
          a gift to the Person
        </p>
        <p>
          <CopyBlock
            text={`gift_list = people_who_deserve_gifts()

for person in gift_list:
    deliver_gift(person)
    print("Gift delivered to:", person)
print("All gifts delivered")`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Das meiste sollte Ihnen bekannt vorkommen. Hier rufen wir zwei
          Funktionen auf: people_who_deserve_gifts() und deliver_gift() - ihre
          Implementierung bleibt ein Geheimnis, das nur der Weihnachtsmann
          kennt. Das Ergebnis der ersten Funktion kann gift_list genannt werden,
          damit wir später auf diesen Wert verweisen können.
        </p>
        <h3>Schleife:</h3>
        <ul>
          <li>
            Wort <b>for</b>
          </li>
          <li>
            Name der temporären Variablen, die den nachfolgenden Elementen
            zugewiesen werden soll
          </li>
          <li>das Wort in</li>
          <li>
            der Wert der Liste oder eines anderen Elements, nach dem wir
            iterieren werden
          </li>
          <li>
            der eingerückte Inhalt, der innerhalb der Schleife ausgeführt wird.
          </li>
          <p>
            Mit einer Liste können wir die erste Hälfte des Weihnachtsbaums
            anzeigen:
          </p>
        </ul>
        <p>
          <CopyBlock
            text={`lst = [1, 2, 3]
for n in lst:
  print("*"*n)
 
*
**
***`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Aber warum eine Liste verwenden, wenn wir einen range()-Iterator
          haben. Wir können seine Funktion jederzeit mit abrufen: help(range)
          einige Beispiele, wie range funktioniert:
        </p>
        <p>
          <CopyBlock
            text={`list(range(2, 5, 1))
[2, 3, 4]
list(range(1, 11, 2))
[1, 3, 5, 7, 9]
list(range(1, 11))
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
list(range(1, 2))
[1]
list(range(2))
[0, 1]`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Die Funktion range() erstellt nicht direkt eine Liste von Elementen,
          sondern gibt einen Iterator zurück. Generatoren erzeugen die Elemente
          einer Folge nacheinander und vermeiden so die Speicherung der gesamten
          Folge im Speicher. Um eine Liste von Elementen zu erhalten, verwenden
          wir die Funktion list(). Wenn wir den Cast von list() weglassen, sieht
          das Ergebnis etwa so aus:
        </p>
        <p>
          <CopyBlock
            text={`range(4)
range(0, 4)`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>Lasst uns den halben Weihnachtsbaum ausgeben:</p>
        <p>
          <CopyBlock
            text={`lst = list(range(1, 11))
lst
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for i in lst:
   print("*"*i)
*
**
***
****
*****
******
*******
********
*********
**********`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Die Funktion range() hat uns eine Menge Zeit erspart. Wir können noch
          mehr sparen, wenn wir die Benennung der Liste überspringen:
        </p>
        <p>
          <CopyBlock
            text={`for i in list(range(1, 5)):
            print(i*"#")
#
##
###
####`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Nichts hindert Sie daran, eine Schleife innerhalb einer anderen
          Schleife zu platzieren, also los geht&apos;s! Denken Sie nur daran,
          eine korrekte Einrückung und unterschiedliche Namen zu verwenden, z.
          B. i und j (oder andere, die sich auf den Inhalt der iterierten Liste
          beziehen):
        </p>
        <p>
          <CopyBlock
            text={`for i in range(1, 3):
    for j in range(11, 14):
        print(i, j)
1 11
1 12
1 13
2 11
2 12
2 13`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Hier haben wir eine innere Schleife, die die Elemente von 11 bis 13
          durchläuft (14 ist bei der Verwendung von range nicht enthalten), und
          eine äußere Schleife, die von 1 bis 2 durchläuft. Wie Sie sehen,
          werden die Elemente der inneren Schleife bei jeder Iteration der
          äußeren Schleife zweimal ausgegeben. Auf diese Weise können wir die
          Teile des Weihnachtsbaums anzeigen:
        </p>
        <p>
          <CopyBlock
            text={`for i in range(3): # repeats 3 times
    for size in range(1, 4):
        print(size*"*")
*
**
***
*
**
***
*
**
***`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Bevor wir mit dem nächsten Kapitel fortfahren, erstellen wir die Datei
          xmas.py und verschieben unseren Code dorthin.
        </p>
        <h2>Definition der Funktion</h2>
        <p>
          Wir haben bereits gesehen, wie wvuded-Funktionen viele unserer
          Probleme lösen. Allerdings lösen sie nicht alle unsere Probleme.
          Manchmal müssen wir das Problem selbst lösen. Wenn dies in unserem
          Programm häufig vorkommt, wäre es schön, wenn wir eine eigene Funktion
          hätten, die das Problem für uns löst. Funktionen in Python sehen
          vertraut aus:
        </p>
        <p>
          <CopyBlock
            text={`def print_triangle(n):
    for size in range(1, n+1):
        print(size*"*")

print_triangle(3)
*
**
***
print_triangle(5)
*
**
***
****
*****`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>Schauen wir uns diese Funktion print_triangle() genauer an:</p>
        <p>
          <CopyBlock
            text={`def print_triangle(n):
            for size in range(1, n+1):
                print(size*"*")`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Die Definition einer Funktion beginnt immer mit dem Wort def. Dann
          folgt ihr Name, zwischen den Klammern können die Argumente der
          Funktion angegeben werden. Man sollte immer so viele Parameter
          (Variablen) an die Funktion übergeben, wie die Funktion erwartet:)
        </p>
        <p>
          <CopyBlock
            text={`def uga(a, b, c):
    print("Uga", a, b, c)`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>Wie bei allen Funktionen, die wir zuvor aufgerufen haben</p>
        <p>
          <CopyBlock
            text={`uga(1, "Ala", 2 + 3 + 4)
Uga 1 Ala 9
x = 42
uga(x, x + 1, x + 2)
Uga 42 43 44`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Beachten Sie, dass der Argumentname nur eine Bezeichnung ist. Wenn wir
          den Wert unter einem Label ändern, ändern sich die anderen Werte nicht
          - das Gleiche passiert mit Funktionsargumenten:
        </p>
        <p>
          <CopyBlock
            text={`def plus_five(n):
    n = n + 5
    print(n)
x = 43
plus_five(x)
48
prnt(x)
43`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Wir sehen eine normale Variable namens x. Zwei Dinge sind hier jedoch
          wichtig zu beachten:
        </p>
        <p>
          Erstens werden die Argumente der Funktion jedes Mal definiert, wenn
          die Funktion aufgerufen wird (unser n ), und Python hängt die
          entsprechenden Argumentwerte an jeden der Argumentnamen an, die es
          gerade erstellt hat. Das heißt, unter n wird der in Klammern gesetzte
          Wert - x angehängt.
        </p>
        <p>
          Zweitens sind die Namen der Argumente außerhalb der Funktion nicht
          verfügbar, da sie beim Aufruf der Funktion erstellt werden und nach
          Beendigung des Aufrufs vergessen werden. Wenn Sie also versuchen, auf
          eine Variable n zuzugreifen, die in der Funktion plus_five(), aber
          außerhalb des Funktionscodes definiert ist, wird Python antworten,
          dass die Variable undefiniert ist:
        </p>
        <p>
          <CopyBlock
            text={`>>> n
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
NameError: name 'n' is not defined`}
            theme={monokai}
            language="py"
          />
        </p>
        <h2>Rückgabe von Werten</h2>
        <p>
          Eingebaute Funktionen haben eine wichtige Eigenschaft, die der oben
          erstellten Funktion fehlt - die eingebauten Funktionen geben den
          geänderten Wert zurück, anstatt ihn sofort auszugeben. Um den gleichen
          Effekt zu erreichen - return. Wir verwenden diese Anweisung nur bei
          Funktionen. Zum Beispiel können wir unseren BMI-Zählungscode
          verbessern, indem wir eine Funktion hinzufügen:
        </p>
        <p>
          <CopyBlock
            text={`def calc_bmi(height, weight):
    return weight / height ** 2`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Als letztes Beispiel für die Funktion ist hier die Lösung des Problems
          vom Ende des vorherigen Abschnitts:
        </p>
        <p>
          <CopyBlock
            text={`def print_triangle(n):
    for size in range(1, n+1):
        print(size * "*")

for i in range(2, 5):
    print_triangle(i)`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          <CopyBlock
            text={`*
**
*
**
***
*
**
***
****`}
            theme={monokai}
            language="py"
          />
        </p>
        <h2>Der ganze Weihnachtsbaum</h2>
        <p>
          Das vorangegangene Kapitel war recht theoretisch, daher werden wir nun
          einige unserer Kenntnisse über Funktionen anwenden, um das Programm zu
          vervollständigen und den Weihnachtsbaum darzustellen.
        </p>
        <p>Für das Protokoll:</p>
        <p>
          <CopyBlock
            text={`# xmas.py

def print_triangle(n):
    for size in range(1, n+1):
        print(size * "*")
            
for i in range(2, 5):
    print_triangle(i)`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          So ändern Sie print_triangle(), um ein Weihnachtsbaumsegment statt der
          Hälfte anzuzeigen. Definieren wir zunächst, wie unser Ergebnis bei
          einem gegebenen Parameter n aussehen soll. Vermutlich legt n die
          Breite fest. Z.B. n = 5, sollte uns zurückgeben:
        </p>
        <p>
          <CopyBlock
            text={`  *
 ***
*****`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Es ist zu beachten, dass jede Zeile aus zwei weiteren Sternchen
          besteht als die vorherige. Wir können also das dritte Argument der
          Funktion range() verwenden:
        </p>
        <p>
          <CopyBlock
            text={`def print_segment(n):
    for size in range(1, n+1, 2):
        print(size * "*")

print_segment(5)`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          <CopyBlock
            text={`*
***
*****`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Das ist nicht ganz das, was wir tun wollen. Die Methode
          string.centre() aus früheren Lektionen wird uns helfen:
        </p>
        <p>
          <CopyBlock
            text={`def print_segment(n):
    for size in range(1, n+1, 2):
        print((size * "*").center(n))

print_segment(5)`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          <CopyBlock
            text={`  *
 ***
*****`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>Es hat sich jedoch ein neues Problem ergeben:</p>
        <p>
          <CopyBlock
            text={`def print_segment(n):
    for size in range(1, n+1, 2):
        print((size * "*").center(n))
        
for i in range(3, 8, 2):
    print_segment(i)`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          <CopyBlock
            text={`
 *
***
  *
 ***
*****
   *
  ***
 *****
*******`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Wenn wir die Größe des breitesten Segments im Voraus kennen, können
          wir ein zusätzliches Argument zu print_segment() hinzufügen, um die
          Breite auszurichten. Versuchen wir nun, das Wissen, das wir bereits
          über unsere Funktion haben, zu kombinieren:
        </p>
        <p>
          <CopyBlock
            text={`def print_segment(n, total_width):
    for size in range(1, n+1, 2):
        print((size * "*").center(total_width))

def print_tree(size):
    for i in range(3, size+1, 2):
        print_segment(i, size)

print("Choose size of the Christmas tree:")
n = int(input())
print_tree(n)`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          <CopyBlock
            text={`Choose size of the Christmas tree:
7
   *
  ***
   *
  ***
 *****
   *
  ***
 *****
*******`}
            theme={monokai}
            language="py"
          />
        </p>
      </article>
    </>
  );
}
