import { CodeBlock, CopyBlock, monokai } from 'react-code-blocks';

export default function Functions() {
  return (
    <>
      <header>
        <h1>Funktionen</h1>
      </header>
      {/*       <nav>
        <a href="#">Home</a>
        <a href="#">Articles</a>
        <a href="#">Contact</a>
        <a href="#">About Us</a>
      </nav> */}
      <article>
        <p>
          Kleinere &quot;Sub-Algorithmen&quot;, die wir selbst schreiben, um den
          Code in kleinere Teile zu zerlegen.
        </p>
        <p>
          Oder wir verwenden Standardfunktionen und -methoden, um eine
          Teilaufgabe zu lösen. Dies gilt natürlich nur für Funktionen, die
          bereits geschrieben und in den Python-Bibliotheken verfügbar sind, wie
          print().
        </p>
        <h2>Standardfunktionen</h2>
        <p>
          Standardfunktionen sind eine Reihe von Funktionen, die dem
          Python-Interpreter immer zur Verfügung stehen.
          <img
            src="/images/buildin.png"
            alt="Description"
            style={{ width: '100%', height: 'auto' }}
          />
        </p>
        <p>
          Bislang haben wir uns mit einigen davon vertraut gemacht.
          Standardfunktionen für die Eingabe input() und die Ausgabe print().
        </p>
        <p>Funktionen im Zusammenhang mit der Typenzuordnung:</p>
        <ul>
          <li>int()</li>
          <li>float()</li>
          <li>komplex()</li>
          <li>str()</li>
          <li>bool()</li>
          <li>list()</li>
          <li>tuple()</li>
          <li>set()</li>
          <li>dict()</li>
        </ul>
        <p>
          ... und noch einige andere, denen wir nicht immer genug Aufmerksamkeit
          schenken:
        </p>
        <ul>
          <li>type()</li>
          <li>len()</li>
          <li>bereich()</li>
          <li>round()</li>
          <li>format()</li>
          <li>isinstance()</li>
          <li>bin()</li>
          <li>oct()</li>
          <li>hex()</li>
          <li>abs()</li>
          <li>min()</li>
          <li>max()</li>
        </ul>
        <h2>Funktion, Procedur, Methode</h2>
        <p>
          Heutzutage werden diese Begriffe in objektorientierten Sprachen
          austauschbar verwendet (wobei der Name &quot;Methode&quot;
          hervorgehoben wird), aber historisch gesehen gibt es Unterschiede
          zwischen ihnen:
        </p>
        <ul>
          <li>
            <b>Funktion</b> - gibt einen Wert oder Daten zurück. Das heißt, der
            letzte Befehl könnte return &quot;irgendwas&quot; sein. Eine
            Funktion gibt ein Ergebnis an die Stelle im Code zurück, an der sie
            aufgerufen wurde.
          </li>
          <li>
            <b>Prozedur</b> - in einfachen Worten: eine Funktion, die nichts
            zurückgibt. Das heißt, ein kompaktes, funktionales Stück Code, das
            die Ergebnisse seiner Arbeit auf andere Weise als durch den
            Return-Befehl zur Verfügung stellt (nur weil sie nichts zurückgibt,
            heißt das nicht, dass sie nichts tut!).
          </li>
          <li>
            <b>Methode</b> - beide oben genannten Bezeichnungen, die am
            häufigsten in objektorientierten Sprachen verwendet werden. Von
            Klassen (einem Element der objektorientierten Programmierung) sagt
            man (richtigerweise), dass sie Methoden haben (nicht: Funktionen
            oder Prozeduren). Die Bennung ist eine Frage der Konvention.
          </li>
        </ul>
        <p>
          Compilern und Interpretern ist es ziemlich egal, wie der Programmierer
          solche isolierten Codefragmente für sich selbst benennt. (Funktion =
          Prozedur = Methode ← ist eine Vereinfachung, aber das reicht für den
          Moment).
        </p>
        <p>
          Eine einfache Definition könnte so lauten:
          <i>
            &quot;Wir behandeln eine Funktion als einen isolierten Codeblock mit
            einem Namen, der in einem Programm, durch den Bezug auf den Namen,
            wiederholt verwendet werden kann.&quot;
          </i>
        </p>
        <CodeBlock
          text={`spell = 'Abrakadabra'
len(spell) # eine Standardfunktion
11
spell.upper() # Anwendung einer Methode auf eine String-Variable
'ABRAKADABRA'`}
          theme={monokai}
          language="sh"
        />
        <h2>Funktionen erstellen</h2>
        <p>
          Funktionen gruppieren Anweisungen in Blöcken als eine Liste von
          Befehlen, die eine bestimmte Aufgabe erfüllen. Funktionen helfen
          dabei, unser Programm in kleinere und modulare Teile zu zerlegen. Wenn
          unser Programm wächst, machen Funktionen den Code übersichtlicher und
          der code kann leichter verwaltet werden. Außerdem werden durch die
          Aufteilung der Anweisungen in Funktionen Wiederholungen vermieden und
          der Code wird wiederverwendbar.
        </p>
        <h3>Syntax</h3>
        <p>
          In Python erstellen wir Funktionen mit dem Schlüsselwort def, geben
          ihnen einen Namen und schließen sie mit Klammern () ab. Wir verwenden
          :, um einen Block zu beginnen. Ein Block kann wie immer in Python mit
          4 Leerzeichen oder Tabulatoren verschachtelt werden.
          <CopyBlock
            text={`def greet():
    print('Good morning!')`}
            theme={monokai}
            language="py"
          />
        </p>
        <p>
          Sobald eine Funktion definiert ist, können wir sie aufrufen:
          <CodeBlock
            text={`greet()
'Good morning!'`}
            theme={monokai}
            language="sh"
          />
        </p>
        <p>
          Funktionen können Parameter annehmen:
          <CopyBlock
            text={`def greet(name):
    print('Good morning!', name)`}
            theme={monokai}
            language="py"
          />
          <CodeBlock
            text={`greet('Vicky')
'Good morning! Vicky'`}
            theme={monokai}
            language="sh"
          />
        </p>
        <h3>Docstring</h3>
        <p>
          Die erste nicht zugewiesene Zeichenkette nach dem Funktionskopf heißt
          docstring und ist die Abkürzung für Dokumentationsstring. Er dient
          dazu, auf einen Blick zu erklären, was die Funktion tut. Das
          Hinzufügen eines docstring ist optional, aber es ist nützlich, eine
          solche Dokumentation an vielen Stellen des Codes zu haben.
          <CopyBlock
            text={`def square(a):
    """This function calculates the square area using sides value passed as parameter"""
    print("Square: ", a * a)`}
            theme={monokai}
            language="py"
          />
          <CodeBlock
            text={`print(square.__doc__)
'This function calculates the square area using sides value passed as parameter'`}
            theme={monokai}
            language="py"
          />
        </p>
        <h3>Return</h3>
        <p>
          Die return-Anweisung beendet die Funktion dort, wo sie sich befindet,
          mit dem aktuellen Ergebnis. Sie wird verwendet, um die Funktion zu
          beenden und zu der Stelle zurückzukehren, von der aus sie aufgerufen
          wurde. Verdeutlichen wir dies anhand eines Beispiels.
        </p>
        <p>
          Obwohl wir eine eingebaute abs()-Funktion in Python haben, wollen wir
          unsere eigene Funktion schreiben, die einen absoluten Wert zurückgibt.
        </p>
        <CopyBlock
          text={`def absolute_value(num):
    """This function returns the absolute value of the entered number"""

    if num >= 0:
        return num
    else:
        return -num
      
    print(absolute_value(4))
    print(absolute_value(-5))`}
          theme={monokai}
          language="py"
        />
        <p>
          Wenn es keinen Ausdruck gibt, den return zurückgeben könnte, oder die
          return-Anweisung selbst nicht in der Funktion vorhanden ist, gibt die
          Funktion ein None-Objekt zurück.
        </p>
        <CopyBlock
          text={`def greet(name):
    print('Good morning!', name)
    return
      
    print(greet("Vicky"))`}
          theme={monokai}
          language="py"
        />
        <CodeBlock
          text={`Good morning! Vicky
None`}
          theme={monokai}
          language="py"
        />
        <CopyBlock
          text={`def greet(name):
    print('Good morning!', name)
      
    print(greet("Vicky"))`}
          theme={monokai}
          language="py"
        />
        <CodeBlock
          text={`Good morning! May
None`}
          theme={monokai}
          language="py"
        />
        <p>
          <ul>
            <li>Was ist mit den Funktionen, die wir bereits kennen?</li>
            <li>
              print() - unterschiedliche Anzahl von Parametern, gibt keinen Wert
              zurück
            </li>
            <li>type() - ein Parameter</li>
            <li>
              int(), str(), list() - Funktionen, die ein Objekt erzeugen, geben
              einen Wert zurück
            </li>
            <li>len() - ein Parameter, gibt einen Wert vom Typ int zurück</li>
          </ul>
        </p>
        <h2>Aufgaben:</h2>
        <p>
          <ol>
            <li>
              Schreiben Sie eine Funktion, die den Flächeninhalt eines Kreises
              anhand eines gegebenen Radius berechnet.
            </li>
            <li>
              Schreiben Sie eine Funktion, die prüft, ob eine Zahl gerade ist.
            </li>
            <li>
              Schreiben Sie eine Funktion, die eine Liste von Zahlen annimmt und
              die Summe aller Elemente der Liste zurückgibt.
            </li>
            <li>
              Schreiben Sie eine Funktion, die alle geraden Zahlen in der
              übergebenen Liste von Elementen ausgibt (verwenden Sie die
              Funktionen aus Punkt 2).
            </li>
          </ol>
        </p>
        <h2>Anwendungsbereich von Variablen</h2>
        <p>
          Variablen können nur in dem Bereich verwendet werden, in dem sie
          definiert sind, was als Geltungsbereich bezeichnet wird. Betrachten
          Sie ihn als den Bereich des Codes, in dem Variablen verwendet werden
          können. Python unterstützt globale Variablen (die im gesamten Programm
          verfügbar sind) und lokale Variablen (die nur in dem Bereich verfügbar
          sind, den die Funktionen kennen). Standardmäßig sind alle Variablen,
          die innerhalb einer Funktion deklariert werden, lokale Variablen.
        </p>
        <CopyBlock
          text={`var = "global"

def my_func():
    print("var inside :", var)
          
my_func()
    print("var outside:", var)`}
          theme={monokai}
          language="py"
        />
        <p>Zmienna globalna vs localna:</p>
        <CopyBlock
          text={`var = "global"

def my_func():
    var = "local"
    print("var inside my_func():", var)
          
def my_func2():
    print("var inside my_func2():", var)
          
my_func()
my_func2()
print("var outside:", var)`}
          theme={monokai}
          language="py"
        />
        <p>
          Lokale Variablen - werden innerhalb dieser Funktion deklariert und
          hören auf zu existieren, wenn die Funktion beendet ist.
        </p>
        <p>
          Globale Variable - sichtbar (und zugänglich) für eine oder mehrere
          Funktionen. Globale Variablen sollten vor den Funktionsdeklarationen,
          in denen sie verwendet werden sollen, deklariert werden (sie sollten
          am Anfang des Codes in der Datei stehen).
        </p>
        <p>Ein weiteres Beispiel wäre die Änderung unserer Funktion greet():</p>
        <CopyBlock
          text={`p = 'Hello!' # p globale Variable

def greet(name):
    name.capitalize() # name lokale Variable
    print(p, '~~~', name)
          
girl = 'anna'
boy = 'marc'
greet(girl)
greet(boy)`}
          theme={monokai}
          language="py"
        />
        <p>
          Wenn wir versuchen, auf eine Variable zuzugreifen, die nicht
          deklariert ist, wenn die Funktion aufgerufen wird, meldet Python einen
          Fehler:
        </p>
        <CopyBlock
          text={`def rectangle(x, y):
    print('x * y = ' + str(x * y))
    z = 4 # weil die Variable z nicht verfügbar ist, funktioniert diese Zuordnung NICHT
      
z = 3
rectangle(3, 2)
print(z)`}
          theme={monokai}
          language="py"
        />
        <p>
          Wie können wir den Fehler beheben? Wenn wir uns nur um die Reihenfolge
          der Anzeige kümmern, können wir die Variable innerhalb unserer
          Funktion platzieren:
        </p>
        <CopyBlock
          text={`def rectangle(x, y):
    print('x * y = ' + str(x * y))
    z = 4 # die Variable wird in der Funktion deklariert, in der sie verwendet wird
    print(z)
      
rectangle(3, 2)`}
          theme={monokai}
          language="py"
        />
        <p>
          Wenn wir eine Variable global verfügbar machen wollen, müssen wir sie
          vor der Verwendung deklarieren.
        </p>
        <CopyBlock
          text={`z = 1

def rectangle(x, y):
    print('x * y = ' + str(x * y))
    z = 4 # die Variable wird in der Funktion deklariert, in der sie verwendet wird
          
rectangle(3, 2)
print(z)`}
          theme={monokai}
          language="py"
        />
        <h2>Aufgaben</h2>
        <p>
          <ol>
            <li>
              Verwenden Sie Ihren bmi.py-Code. Zerlegen Sie die BMI-Zählung in
              eine Funktion, die den BMI aus den Benutzerdaten berechnet und den
              entsprechenden Wert (untergewichtig, normalgewichtig,
              übergewichtig, fettleibig) je nach dem erhaltenen Parameter
              zurückgibt.
            </li>
            <li>
              Schreiben Sie eine Funktion, die das Minimum von 3 Zahlen findet,
              ohne die eingebaute Funktion min() zu verwenden. minimum_of(a, b,
              c).
            </li>
            <li>
              Schreiben Sie eine Funktion, die den Maximalwert von 3 Zahlen
              ermittelt, ohne die eingebaute Funktion max() zu verwenden.
              maximum_of(a, b, c).
            </li>
            <li>
              Schreiben Sie eine Funktion, die prüft, ob eine Zahl in dem vom
              Benutzer angegebenen Bereich vorkommt. Sie soll eine Meldung
              zurückgeben: &quot;ja, die Zahl x liegt in dem angegebenen
              Bereich&quot;, &quot;nein, die Zahl x liegt außerhalb des
              Bereichs&quot;.
            </li>
            <li>
              Schreiben Sie das Wärme-Kälte-Spiel so, dass Sie die Funktion
              verwenden.
            </li>
            <li>
              Schreiben Sie ein Stein-Papier-Schere-Spiel, bei dem Sie die
              Funktionen verwenden.
            </li>
            <li>
              Schreiben Sie ein Programm, das anhand der Kartennummer antwortet,
              ob es sich um Visa, MasterCard oder vielleicht AmericanExpress
              handelt.
              <i>Was wissen wir über diese Kartennummern?</i>
              <ul>
                <li>
                  Alle Visa-Kartennummern beginnen mit einer 4. Neue Karten
                  haben 16 Ziffern. Alte Karten haben 13.
                </li>
                <li>
                  MasterCard-Kartennummern beginnen entweder mit den Ziffern 51
                  bis 55 oder mit den Ziffern 2221 bis 2720. Alle haben 16
                  Ziffern.
                </li>
                <li>
                  American Express-Kartennummern beginnen mit 34 oder 37 und
                  haben 15 Ziffern.
                </li>
              </ul>
            </li>
            <li>
              Schreiben Sie ein Programm, das prüft, ob unser Auto als
              historisch eingetragen werden kann.
              <ul>
                <li>Marke (str)</li>
                <li>Modell (str)</li>
                <li>Jahrgang (int)</li>
              </ul>
              <li>
                Gibt dieses dictionary auf dem Bildschirm aus (ohne jegliche
                Formatierung)
              </li>
              <li>
                Es wird geprüft, ob das Auto mindestens 25 Jahre alt ist. Ist
                dies der Fall, wird die Meldung ausgegeben: &quot;Herzlichen
                Glückwunsch! Ihr Auto (hier_marke) kann als historisch
                registriert werden.&quot;
              </li>
              <li>
                Wenn es die oben genannte Bedingung nicht erfüllt, wird die
                Meldung ausgegeben: &quot;Ihr Auto (hier_marke) ist noch zu
                jung&quot;.
              </li>
              <li>
                Wenn das Programm korrekt funktioniert, ändern Sie die
                dictionary Werte (aber nicht die Schlüssel!), um zu sehen, ob
                das Programm auch sein Verhalten ändert.
              </li>
            </li>
            <li>
              Eine weitere Bedingung für den Erhalt von &quot;gelben
              Schildern&quot; ist, dass das Fahrzeug zu mindestens 75 % aus
              Originalteilen besteht. In unserer Übung gehen wir davon aus, dass
              der Gutachter die Originalität in der Kategorie ja/nein bestimmt
              hat.
            </li>
            <ul>
              <li>
                Im Folgenden wird das dictionary in der obigen Aufgabe erstellt
                und angezeigt:
              </li>
              <ul>
                <li>
                  dem Wörterbuch einen neuen Schlüssel whether_original
                  hinzufügen und seinen Wert (Typ: bool) wie gewünscht
                  festlegen.
                </li>
                <li>das geänderte Wörterbuch erneut anzeigen</li>
              </ul>
              <li>
                Ändern Sie das Programm, um der Entscheidung Rechnung zu tragen,
                dass ein Auto auch nach seiner Originalität zugelassen werden
                kann. Fügen Sie die entsprechenden Meldungen hinzu.
              </li>
            </ul>
            <li>Erstellen Sie ein Spiel zum Worte raten.</li>
            <ul>
              <li>
                Der Computer zieht ein Wort aus einer Liste von Wörtern, die im
                Programm vorhanden sind.
              </li>
              <li>
                Er zeigt ein maskiertes Wort mit einer sichtbaren Anzahl von
                Zeichen an (z. B. &quot;- - - - - - -&quot;).
              </li>
              <li>Der Benutzer gibt einen Buchstaben ein.</li>
              <li>
                Es wird geprüft, ob der Buchstabe in dem Wort vorhanden ist.
                Wenn ja, wird ihm/ihr die Meldung angezeigt:
                <ul>
                  <li>
                    &quot;Treffer!&quot; und eine Beschriftung mit bekannten
                    Buchstaben.
                  </li>
                </ul>
              </li>
              <li>
                Wenn nicht, zeigen Sie die Meldung an:
                <ul>
                  <li>
                    &quot;Nicht getroffen, versuche es noch einmal!&quot;.
                  </li>
                </ul>
              </li>
              <li>
                Sie können die Anzahl der Versuche auf z. B. 10 begrenzen.
              </li>
            </ul>
          </ol>
        </p>
      </article>
    </>
  );
}
