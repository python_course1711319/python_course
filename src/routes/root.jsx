import { Outlet, Link } from 'react-router-dom';
// import { useTranslation } from 'react-i18next';
// import { useState } from 'react';

// const lngs = {
//   en: { nativeName: 'English' },
//   de: { nativeName: 'Deutsch' },
// };

export default function Root() {
  // const { t, i18n } = useTranslation();
  // const [count, setCounter] = useState(0);

  return (
    <>
      <div id="sidebar">
        <h1>Python Course</h1>
        <nav>
          {/* <div>
            {Object.keys(lngs).map((lng) => (
              <button
                key={lng}
                style={{
                  fontWeight: i18n.resolvedLanguage === lng ? 'bold' : 'normal',
                }}
                type="submit"
                onClick={() => {
                  i18n.changeLanguage(lng);
                  setCounter(count + 1);
                }}
              >
                {lngs[lng].nativeName}
              </button>
            ))}
          </div> */}
          {/* <p>
            <i>{t('counter', { count })}</i>
          </p> */}
          <ul>
            <li>
              <Link to="/functions">Functions</Link>
              <Link to="/Weihnachtsbaum">Weihnachtsbaum</Link>
              {/* <Link to="/2">Chapter 2</Link>
              <Link to="/3">Chapter 3</Link> */}
            </li>
          </ul>
        </nav>
      </div>
      <div id="detail">
        <Outlet />
      </div>
    </>
  );
}
