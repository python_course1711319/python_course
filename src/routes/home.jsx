import { useTranslation, Trans } from 'react-i18next';
import { DateTime } from 'luxon';

const getGreetingTime = (d = DateTime.now()) => {
  const split_afternoon = 12; // 24hr time to split the afternoon
  const split_evening = 17; // 24hr time to split the evening
  const currentHour = parseFloat(d.toFormat('hh'));

  if (currentHour >= split_afternoon && currentHour <= split_evening) {
    return 'afternoon';
  } else if (currentHour >= split_evening) {
    return 'evening';
  }
  return 'morning';
};

export default function Home() {
  const { t } = useTranslation();

  return (
    <>
      <header>
        <h1>Python Grundlagen</h1>
      </header>
      {/*       <nav>
        <a href="#">Home</a>
        <a href="#">Articles</a>
        <a href="#">Contact</a>
        <a href="#">About Us</a>
      </nav> */}
      <article>
        <h2>Workshop Einführung in Python</h2>
        <p>Erstellt am 08.05.2024</p>
        <p>
          Dieses Handbuch ist Teil des Workshops Python Grundlagen, den ich im
          Rahmen meiner Arbeit halte. Er enthält die Lektion des Tages und
          Materialien für das Selbststudium.
        </p>
        <h3>Für wen ist dieser Kurs gedacht?</h3>
        <p>
          Sowohl der Kurs als auch das Handbuch sind speziell für Anfänger, die
          Teilnehmer des Workshops, geschrieben worden. Ich habe versucht, die
          aufeinanderfolgenden Lektionen so klar und verständlich wie möglich zu
          schreiben. Um mit Hilfe des Handbuchs zu lernen, benötigen Sie außer
          grundlegenden Computerkenntnissen (einschließlich der Fähigkeit, einen
          Webbrowser zu benutzen) keine fortgeschrittenen technischen
          Kenntnisse.
        </p>
        <p>Englischkenntnisse sind bei der Lösung der Aufgaben hilfreich.</p>
        <p>
          Wenn Sie jedoch Probleme mit einem Thema haben oder etwas
          unverständlich erscheint, schreiben Sie an{' '}
          <a href="mailto:mail@hkiepe.de">mail@hkiepe.de</a>
        </p>
        <p>
          <Trans i18nKey="description.part1">
            Edit <code>src/App.js</code> and save to reload.
          </Trans>
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          {t('description.part2')}
        </a>
      </article>
      <footer>
        <p>
          © {new Date().getFullYear()} Henrik Kiepe. Alle Rechte vorbehalten.
        </p>
        <div>
          {t('footer.date', { date: new Date(), context: getGreetingTime() })}
        </div>
      </footer>
    </>
  );
}
