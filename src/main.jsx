import * as React from 'react';
import * as ReactDOM from 'react-dom/client';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './index.css';
import './i18n';
import Root from './routes/root';
import ErrorPage from './error-page';
import Home from './routes/home';
import Functions from './routes/topics/functions';
import ChristmasTree from './routes/topics/christmasTree';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      { path: '/', element: <Home /> },
      {
        path: '/functions',
        element: <Functions />,
      },
      {
        path: '/weihnachtsbaum',
        element: <ChristmasTree />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
